using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.Runtime.InteropServices;

public static class ProcessLauncher {

	public static IEnumerator Launch(string name) {

		Process process = new Process();
		
		process.StartInfo.FileName = name + ".exe";
		
		
		//process.StartInfo.Arguments = "-n";
		//process.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;

		
		
		//not sure if the ".\" syntax is needed, but it works, soooo
		process.StartInfo.WorkingDirectory = ".\\" + name;

		process.Start();
		
		/*
		UnityEngine.Debug.Log("yielding...");
		
		yield return new WaitForSeconds(2);
		
		UnityEngine.Debug.Log("back.");
		
		SetFocus("THRUSTBURST");
		
		process.WaitForExit();
		*/
		
		yield return null;


	}

	/*
	
	private const int SW_SHOWNORMAL = 1;
	private const int SW_SHOWMINIMIZED = 2;
	private const int SW_SHOWMAXIMIZED = 3;

	
	
	[DllImport("user32.dll")]
	private static extern bool ShowWindowAsync(System.IntPtr hWnd, int nCmdShow);
	
	// SW_SHOWMAXIMIZED to maximize the window
	// SW_SHOWMINIMIZED to minimize the window
	// SW_SHOWNORMAL to make the window be normal size
	//ShowWindowAsync(hWnd, SW_SHOWMINIMIZED);
	
	*/
	
	/*
	
	[DllImport("user32.dll")]
	private static extern System.IntPtr FindWindow(string sClassName, string sAppName);
	
	[DllImport("user32.dll")]
	private static extern bool SetForegroundWindow(System.IntPtr hwnd);
	
	
	//private static void Minim(object sender, System.EventArgs e)
	private static void SetFocus(string name)
	{
		System.IntPtr hWnd = FindWindow(null, name);
		if (!hWnd.Equals(System.IntPtr.Zero))
		{
		
			SetForegroundWindow(hWnd);
			
		}
		else
		{
			UnityEngine.Debug.Log("cannot find window");
		}
		
	}
	*/

}