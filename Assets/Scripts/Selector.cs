using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class Selector : MonoBehaviour
{
	public Transform[] cards;
	public string[] filenames;

	const float leftmostX = -8.8f;
	const float cardWidth = 8.2f;

	int indexSelected;
	float swapTimeout = 0f;
	float selectTimeout = 0f;

	float[] cardVelocities;

	void Start()
	{
		Screen.showCursor = false;

		//Select a random card
		indexSelected = Random.Range(0, cards.Length);

		cardVelocities = new float[cards.Length];

		// iTween.CameraFadeAdd();
		// iTween.CameraFadeFrom(1f, 1.5f);
	}
	
	void Update()
	{
		if ((Input.GetButton("AnyLeft") && swapTimeout <= 0f) || (Input.GetButtonDown("AnyLeft") && swapTimeout <= 0.3f))
		{
			indexSelected = ( indexSelected == 0 ) ? cards.Length - 1 : indexSelected - 1;
			swapTimeout = 0.5f;
		}
		else if ((Input.GetButton("AnyRight") && swapTimeout <= 0f) || (Input.GetButtonDown("AnyRight") && swapTimeout <= 0.3f))
		{
			indexSelected = (indexSelected + 1) % cards.Length;
			swapTimeout = 0.5f;
		}
		
		if (Input.GetButtonDown("AnyA") || Input.GetButtonDown("AnyB"))
		{
			if(selectTimeout <= 0f)
			{
				string exeName = filenames[indexSelected];
				// to make a card a "dead" card (like an information card) just make its filename = ""
				// TODO: pressing a button on a dead card should probably do SOMETHING...
				if(!string.IsNullOrEmpty(exeName))
				{
					StartCoroutine(ProcessLauncher.Launch(exeName));
					//TODO...
					selectTimeout = 3f;
				}
			}
		}
		
		SetCards();

		swapTimeout -= Time.deltaTime;
		selectTimeout -= Time.deltaTime;
	}

	void SetCards()
	{
		//shunt every card to the left of it, and position the card itself
		for(int i=0; i<indexSelected+1; i++)
		{
			var pos = cards[i].position;
			pos.x = Mathf.SmoothDamp(pos.x, leftmostX + i, ref cardVelocities[i], 0.3f);
			cards[i].position = pos;
		}

		//shunt every card to the right of it.
		for(int i=indexSelected+1; i<cards.Length; i++)
		{
			var pos = cards[i].position;
			pos.x = Mathf.SmoothDamp(pos.x, leftmostX + i + cardWidth, ref cardVelocities[i], 0.3f);
			cards[i].position = pos;
		}
	}

}
